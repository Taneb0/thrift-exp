package uk.ac.york.es.thrift.streams.subscribers;

import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Component that watches over a TProtocol and ensures it stays open.
 * Connections are retried using an exponential backoff scheme (starting at
 * {@link #HALF_INITIAL_RETRY} and then doubling after each failure.
 *
 * @author Antonio Garcia-Dominguez
 */
class ConnectionStatusMonitor {
	private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionStatusMonitor.class);
	private static final int HALF_INITIAL_RETRY = 100;

	private final TProtocol protocol;

	public ConnectionStatusMonitor(TProtocol protocol) {
		this.protocol = protocol;
	}

	public synchronized TProtocol waitForConnection() {
		// Start with 100ms of delay, use exponential backoff
		int delay = HALF_INITIAL_RETRY;

		while (!protocol.getTransport().isOpen()) {
			try {
				protocol.getTransport().open();
				delay = HALF_INITIAL_RETRY;
				LOGGER.info("Connection established");
			} catch (TTransportException e) {
				delay *= 2;
				LOGGER.error(
						String.format(
								"There was an error while opening the connection: waiting for %d ms before retrying",
								delay), e);
				try {
					Thread.sleep(delay);
				} catch (InterruptedException e1) {
					LOGGER.warn("Interrupted wait", e1);
				}
			}
		}

		return protocol;
	}

	public synchronized void disconnect() {
		if (protocol.getTransport().isOpen()) {
			protocol.getTransport().close();
		}
	}
}