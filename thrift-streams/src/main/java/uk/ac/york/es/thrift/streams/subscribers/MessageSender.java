package uk.ac.york.es.thrift.streams.subscribers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.thrift.TServiceClient;
import org.apache.thrift.TServiceClientFactory;
import org.apache.thrift.protocol.TProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Runnable that enqueues outgoing messages, sending them as soon as the connection
 * is ready and relieving the client from having to wait.</p>
 *
 * <p>It is assumed that all the methods in the Thrift service to be invoked are oneway
 * or have <code>void</code> as return type.</p>
 *
 * @author Antonio García-Domínguez
 */
class MessageSender<T extends TServiceClient> implements Runnable {
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageSender.class);

	private volatile boolean running;
	private Thread runningThread;

	private ConnectionStatusMonitor monitor;
	private TServiceClientFactory<T> clientFactory;

	private static class Invocation {
		public final Method method;
		public final Object[] args;

		public Invocation(Method m, Object[] args) {
			this.method = m;
			this.args = args;
		}
	}
	private BlockingQueue<Invocation> queue = new LinkedBlockingQueue<Invocation>();

	public MessageSender(ConnectionStatusMonitor monitor, TServiceClientFactory<T> clientFactory) {
		this.monitor = monitor;
		this.clientFactory = clientFactory;
	}

	public void run() {
		try {
			running = true;
			runningThread = Thread.currentThread();

			/*
			 * We assume here that the TProtocol instance is never going to change
			 * (otherwise, we'd have to recreate the client after each call to
			 * waitConnection).
			 */
			while (running) {
				final TProtocol protocol = monitor.waitForConnection();
				final T client = clientFactory.getClient(protocol);

				final Invocation p = queue.take();
				try {
					p.method.invoke(client, p.args);
				} catch (IllegalAccessException e) {
					LOGGER.error("Invoked method is not accessible", e);
				} catch (IllegalArgumentException e) {
					LOGGER.error(String.format("Arguments for invocation of %s are invalid", p.method.getName()), e);
				} catch (InvocationTargetException e) {
					LOGGER.error("Exception while invoking client: reconnecting and retrying later", e);
					monitor.disconnect();
					queue.add(p);
				}
			}
		} catch (InterruptedException e) {
			LOGGER.error("Wait interrupted in sender", e);
		} finally {
			stop();
		}
	}

	/**
	 * Stops the loop, interrupting any waits on pending invocations and closing the connection.
	 */
	public void stop() {
		running = false;
		runningThread.interrupt();
		monitor.disconnect();
	}

	/**
	 * <p>
	 * Returns a proxy to the specified interface that enqueues all invocations
	 * to be sent to the Thrift service once the connection is established.
	 * </p>
	 * 
	 * @param loader
	 *            Classloader to use to create the proxy.
	 * @param iface
	 *            Interface for the remote service (usually
	 *            <code>MyService.Iface.class</code>).
	 */
	@SuppressWarnings("unchecked")
	public <Iface> Iface getProxy(ClassLoader loader, Class<Iface> iface) {
		return (Iface) Proxy.newProxyInstance(loader, new Class[] {  iface },
			new InvocationHandler() {
				@Override
				public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
					queue.add(new Invocation(method, args));
					return null;
				}
			});
	}

	/**
	 * Convenience version of {@link #getProxy(ClassLoader, Class)} which uses
	 * the same classloader as the interface for the service.
	 */
	public <Iface> Iface getProxy(Class<Iface> iface) {
		return getProxy(iface.getClassLoader(), iface);
	}
}