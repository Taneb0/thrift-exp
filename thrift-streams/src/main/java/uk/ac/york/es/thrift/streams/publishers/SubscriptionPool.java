package uk.ac.york.es.thrift.streams.publishers;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.thrift.TServiceClient;
import org.apache.thrift.TServiceClientFactory;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Pool of subscriptions, with their own filters. This class only takes care of the
 * additions and removals of subscriptions: it is intended to be subclassed by users
 * to provide the desired notifyXXX methods.</p>
 *
 * <p>In particular, these notifyXXX methods should not remove subscriptions while
 * iterating them: for more details, please see {@link #removeSubscription(TServiceClient)}.</p>
 *
 * @author Antonio Garcia-Dominguez
 */
public abstract class SubscriptionPool<T extends TServiceClient, SubscriptionOptions> {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionPool.class);

	public class Subscription {
		private final T client;
		private SubscriptionOptions options;

		public Subscription(T client, SubscriptionOptions options) {
			this.client = client;
			this.options = options;
		}

		public SubscriptionOptions getOptions() {
			return options;
		}

		private void setOptions(SubscriptionOptions options) {
			this.options = options;
		}

		public T getClient() {
			return client;
		}
	}

	private final TServiceClientFactory<T> clientFactory;
	private final Map<TTransport, Subscription> transports = new HashMap<TTransport, Subscription>();

	public SubscriptionPool(TServiceClientFactory<T> clientFactory) {
		this.clientFactory = clientFactory;
	}

	/**
	 * Adds a subscription to the pool for this connection, if it is not already in the pool.
	 * If it is in the pool, it will update the subscription options.
	 */
	public synchronized Subscription addSubscription(TTransport clientTransport, SubscriptionOptions options) {
		Subscription s = transports.get(clientTransport);
		if (s == null) {
			final T client = clientFactory.getClient(new TBinaryProtocol(clientTransport));
			s = new Subscription(client, options);
			transports.put(clientTransport, s);
			LOGGER.debug("Added client {} for transport {}", client, clientTransport);
		} else {
			s.setOptions(options);
			LOGGER.debug("Changed options for client {}", s.getClient());
		}
		return s;
	}

	/**
	 * Removes a subscription from the pool.
	 * 
	 * Note: this method should not be called while iterating over the clients, as that would
	 * produce a ConcurrentModificationException. It's better to collect the clients to be
	 * removed during the loop, and then remove them all in one go using this method or
	 * {@link #removeSubscriptions(Iterable)}.
	 */
	public synchronized void removeSubscription(T client) {
		final TTransport transport = client.getInputProtocol().getTransport();

		// Close the connection and forget about this client
		transport.close();
		transports.remove(transport);

		LOGGER.debug("Removed client {} with transport {} from subscribers", client, transport);
	}

	/**
	 * Convenience version of {@link #removeSubscription(T)} for a collection of clients.
	 */
	public synchronized void removeSubscriptions(Iterable<T> clients) {
		for (T client : clients) {
			removeSubscription(client);
		}
	}

	/**
	 * Returns an unmodifiable, iterable view of all the current subscriptions.
	 */
	public synchronized Iterable<Subscription> getSubscriptions() {
		return Collections.unmodifiableCollection(transports.values());
	}
}
