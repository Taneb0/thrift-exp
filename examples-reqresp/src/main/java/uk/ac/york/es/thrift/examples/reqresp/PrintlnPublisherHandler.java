package uk.ac.york.es.thrift.examples.reqresp;

import org.apache.thrift.TException;

import uk.ac.york.thrift.es.api.Filter;
import uk.ac.york.thrift.es.api.Publisher;

public class PrintlnPublisherHandler implements Publisher.Iface {
	@Override
	public void subscribe(Filter p) throws TException {
		System.out.println(String.format("Got subscription request for '%s'", p.pattern));
	}
}