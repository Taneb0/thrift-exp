Thrift WebSockets experiment
============================

A set of experiments with various transports in Thrift, to see how to implement bidirectional Java-to-Java communication.

Requirements
------------

* Maven 3 or later.

* Install the Thrift compiler somewhere on your $PATH. As I did
  not have root access on my computer, I installed it locally with:

      JAVA_PREFIX=$HOME/.local/lib \
        ./configure --prefix=$HOME/.local \
        --without-cpp --without-lua --without-erlang \
        --without-nodejs --without-ruby --without-php \
        --without-haskell --without-python

  After that, the `thrift` binary was placed on `~/.local/bin` and the
  Thrift JARs were placed on `~/.local/lib`.

Compilation
-----------

Just run `mvn compile`.

Some useful articles and examples
---------------------------------

* [Initial whitepaper by Facebook](https://thrift.apache.org/static/files/thrift-20070401.pdf)
* [DZone quickstart](http://java.dzone.com/articles/apache-thrift-java-quickstart)
* [IDL grammar](https://thrift.apache.org/docs/idl)
* [thrift-over-socketio (Thrift over WebSockets on JS, already merged)](https://github.com/postlock/thrift-over-socketio)
